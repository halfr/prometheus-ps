import os
from setuptools import setup

setup(
    name="prometheus-ps",
    version="0.0.1",
    author="Rémi Audebert",
    author_email="rflah0@gmail.com",
    description="Exports ps metrics for the Prometheus monitoring system.",
    license="Apache",
    keywords="ps monitoring prometheus",
    url="https://bitbucket.org/halfr/prometheus-ps",
    packages=("prometheus_ps",),
    entry_points={'console_scripts': [ 'prometheus-ps = prometheus_ps.main:main' ]},
    install_requires=[
        "prometheus_client",
        "pip-prometheus>=1.0.0",
    ],
    dependency_links = ['https://github.com/prometheus/client_python/zipball/master#egg=prometheus_client'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: Information Technology",
        "Intended Audience :: System Administrators",
        "Topic :: System :: Monitoring",
        "License :: OSI Approved :: Apache Software License",
    ],
)
