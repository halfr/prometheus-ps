# Prometheus journald exporter

Prometheus exporter for current running programs.

## Usage

### Requirements

* `python-systemd`
* `prometheus_client`

### Installation

Install with:

```shell
pip install prometheus-ps
```

### Quickstart

Run:

```shell
prometheus-ps
```

Visit: http://localhost:9010

By default, prometheus-ps

* The size of the journal

### Adding custom rules
