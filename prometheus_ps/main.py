#!/usr/bin/env python3
from http.server import HTTPServer
import argparse
import os
import random
import select
import subprocess
import threading
import time
from collections import Counter

from prometheus_client import Gauge, MetricsHandler

# Export metrics about installed packages
import pip_prometheus

class PrometheusPS(threading.Thread):
    def __init__(self, interval):
        super().__init__()

        self.interval = interval

        self.ps = Gauge('programs',
                        'Count of programs started',
                        ['name', 'user'])

    def get_metrics(self):
        out = subprocess.check_output(['/usr/bin/ps', '-ax', '-o', 'user,cmd']).decode()
        ps = [line.split() for line in out.splitlines()]
        progs = [(p[0], p[1]) for p in ps]
        counter = Counter(progs)
        for (user, prog), count in counter.items():
            self.ps.labels({'name': prog, 'user': user}).set(count)

    def run(self):
        while not time.sleep(self.interval):
            self.get_metrics()


def parse_args():
    parser = argparse.ArgumentParser(description='prometheus-ps')

    default_host = os.environ.get('PROMETHEUS_PS_HOST', '0.0.0.0')
    parser.add_argument('--host', help='listen address', default=default_host)

    default_port = os.environ.get('PROMETHEUS_PS_PORT', '9040')
    parser.add_argument('-p', '--port', type=int, help='listen port',
                        default=int(default_port))

    default_interval = os.environ.get('PROMETHEUS_PS_INTERVAL', '15')
    parser.add_argument('-n', '--interval', type=int, help='specify update interval (s)',
                        default=int(default_interval))

    args = parser.parse_args()
    return args


def main():
    args = parse_args()

    worker = PrometheusPS(args.interval)
    worker.daemon = True
    worker.start()

    httpd = HTTPServer((args.host, args.port), MetricsHandler)
    httpd.serve_forever()

if __name__ == '__main__':
    main()
